from sentence_transformers import SentenceTransformer
import json
from numpy import dot
from numpy.linalg import norm

def load_sbert():
    return SentenceTransformer("/home/jonas/conrec_utils_workspace/conrec_algorithm_work/jobad-sbert/0.1.0-narrow-triplets/trained_model/best_model")

def load_data():
    with open("/home/jonas/prog/hackathon202403/yrken_essential_skills_alla_yrken_version_16.json", "r") as f:
        return json.load(f)


def encode_data_in_place(sbert, data):
    n = len(data)
    for i, x in enumerate(data):
        if i % 100 == 0:
            print(f"Encode {i} of {n}")
        x["vector"] = sbert.encode(x["occupation-name"])

def cossim(a, b):
    return dot(a, b)/(norm(a)*norm(b))

def select_keys(x, ks):
    return {k:x[k] for k in ks}

def compute_candidates(data, v):
    with_score = [{**select_keys(x, ["id", "occupation-name"]),
                   "score": cossim(v, x["vector"])}
                  for x in data]
    with_score.sort(key=lambda x: -x["score"])
    return with_score

def provide_candidates_in_place(data, candidate_count):
    n = len(data)
    for i, x in enumerate(data):
        if i % 100 == 0:
            print(f"Candiates {i} of {n}")
        x["closest_occupations"] = compute_candidates(
            data, x["vector"])[0:candidate_count]

## Transormer

sbert = load_sbert()

## Data

data = load_data()

## Encode the data

encode_data_in_place(sbert, data)

## Provide candidates

provide_candidates_in_place(data, 10)
