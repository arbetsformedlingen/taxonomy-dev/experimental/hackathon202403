#!/usr/bin/env bb
;;
;; This is a Babashka script. Call
;;
;; ./gitlabci_tool.clj help
;;
;; to display help information.
;;
(ns occupation-overlap
  (:require [cheshire.core :as json]
            [clojure.set :as set]
            [babashka.cli :as cli]))

(defn load-json [filename]
  (-> filename
      slurp
      (json/parse-string true)))

(defn jaccard-distance [a b]
  {:pre [(set? a)
         (set? b)]}
  (let [denom (count (set/union a b))
        numer (count (set/intersection a b))]
    (merge {:intersection-size numer
            :union-size denom
            :jaccard-distance 
            (if (zero? denom)
              1
              (- 1.0 (/ numer denom)))})))

(defn provide-skill-ids [data]
  (into []
        (map (fn [occ]
               (assoc occ :skill-ids
                      (into #{}
                            (map :id)
                            (:kompetenser_med_essential_koppling occ)))))
        data))

(defn closest-occupations [occupations-data skill-set]
  (->> occupations-data
       (map (fn [occupation]
              (-> occupation 
                  (merge (jaccard-distance
                          (:skill-ids occupation)
                          skill-set))
                  (dissoc :skill-ids :kompetenser_med_essential_koppling))))
       (sort-by :jaccard-distance)))

(defn provide-closest-occupations [occupations-data candidate-count]
  (let [n (count occupations-data)]
    (into []
          (map-indexed (fn [i occupation]
                         (let [this-id (:id occupation)]
                           (assert (string? this-id))
                           (when (zero? (mod i 100))
                             (println "Provide" i n))
                           (assoc occupation
                                  :closest_occupations
                                  (->> occupation
                                       :skill-ids
                                       (closest-occupations occupations-data)
                                       (remove #(= this-id (:id %)))
                                       (take candidate-count))))))
          occupations-data)))

(defn process-closest-occupations [src-occupations-data candidate-count]
  (-> src-occupations-data
      provide-skill-ids
      (provide-closest-occupations candidate-count)))

(defn process-files [src-filename dst-filename]
  (spit dst-filename
        (-> src-filename
            load-json
            (process-closest-occupations 10)
            json/generate-string)))

(defn default-process [& _]
  (process-files "yrken_essential_skills_alla_yrken_version_16.json"
                 "yrken_essential_skills_alla_yrken_version_16_skill_overlap.json"))

(def table [{:cmds ["process"]
             :fn default-process}])

(defn -main [args]
  (cli/dispatch table args))

(-main *command-line-args*)

(comment


  (def data (load-json "yrken_essential_skills_alla_yrken_version_16.json"))
  (def data2 (provide-skill-ids data))
  (def data3 (provide-closest-occupations data2 10))

  

  )
