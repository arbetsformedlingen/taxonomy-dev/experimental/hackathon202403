# Målformulering Hackathon

## Mål 1

Bygga en prototyp inom kompetensprofilen där kompetenser som du sannolikt har föreslås baserat på yrkeshistorik och yrkespreferenser. Befintlig json-fil med koppling mellan yrken och kompetenser ska vara som bas:

https://data-develop.jobtechdev.se/taxonomy/hackathon/202403/index.html

Kandidatbanken-kodning krävs.

Yrkeshistoriken är en lista av yrkesbenämningar från taxonomin som representerar tidigare erfarenhet. Yrkespreferenser är en lista av yrkesbenämningar som representerar vad man vill jobba som i framtiden.

Resultatet ska vara en lista av kompetenser ur taxonomin som representerar de kompetenser du har.

## Mål 2

Ta fram ett koncept på ett alternativt sätt att fånga kompetenser. Vi kan eventuellt använda semantic concept search:

https://semantic-concept-search-semantic-concept-search-prod.prod.services.jtech.se/

Hur kan man hjälpa till att hitta kompetenser för folk som saknar relevant yrkeshistorik? Hur kan man göra samma sak för personer som har genomgått en viss utbildning nyligen?

Antag att David går arbetsmarknadsutbildning för busschaufför men har inte tidigare arbetat som det. Hur fångar man utbildningens kompetenser?
